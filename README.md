# The LaTeX package `count1to`

Copyright (C) 1995–2009 Martin Schröder

count1to 2024-06-13 v2.11 Count1to LaTeX Package

This is the original release of Count1to. The new upload has only been done to split the ms-bundle.

The current maintainer of this package is Marei Peischl <marei@peitex.de>

***************************************************************************

 This material is subject to the LaTeX Project Public License version 1.3c
 or later. See http://www.latex-project.org/lppl.txt for details.

***************************************************************************

## Provided files

* README.md
* count1to.dtx
* count1to.ins

The files
* count1to.sty
* count1to.drv
can be generated from the count1to.dtx by processing the count1to.ins-file.

